<?php
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();
    if(!empty($_COOKIE['save'])) {
        setcookie('save', '',100000);
        $messages[]= 'Спасибо, результаты сохранены.';
    }

    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['fio_data'] = !empty($_COOKIE['fio_data_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['email_data'] = !empty($_COOKIE['email_data_error']);
    $errors['birthday'] = !empty($_COOKIE['bd_error']);
    $errors['birthday_data'] = !empty($_COOKIE['bd_data_error']);
    $errors['gender'] = !empty($_COOKIE['gen_error']);
    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['check'] = !empty($_COOKIE['check_error']);


    if($errors['fio']) {
        setcookie('fio_error','',100000);
        $messages[] = '<div> Заполните имя</div>';
    }
    if($errors['fio_data']){
        setcookie('fio_data_error','',100000);
        $messages[] = '<div>Разрешено использовать только буквы русского языка(Прописные и строчные)';
    }
    if($errors['email']) {
        setcookie('email_error','',100000);
        $messages[]='<div>Введите почту</div>';
    }
    if($errors['email_data']){
        setcookie('email_data_error','',100000);
        $messages[]='<div>Введите корректную почту</div>';
    }
    if($errors['birthday']) {
        setcookie('bd_error','',100000);
        $messages[]='<div>Введите корректную дату рождения</div>';
    }
    if($errors['birthday_data']){
        setcookie('bd_data_error','',100000);
        $messages[]='<div>Вы не можете быть из будущего</div>';
    }
    if($errors['gender']) {
        setcookie('gen_error','',100000);
        $messages[]='<div>Выберите свой пол</div>';
    }
    if($errors['limbs'])
    {
        setcookie('limbs_error','',100000);
        $messages[] = '<div>Выберите количество конечностей</div>';
    }
    if($errors['bio']) {
        setcookie('bio_error','',100000);
        $messages[]='<div>Расскажите о себе</div>';
    }
    if($errors['check']) {
        setcookie('check_error','',100000);
        $messages[] = '<div>Подтвердите согласие на обработку персональных данных</div>';
    }

    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['birthday'] = empty($_COOKIE['bd_value']) ? '' : $_COOKIE['bd_value'];
    $values['gender'] = empty($_COOKIE['gen_value']) ? '' : $_COOKIE['gen_value'];
    $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
    $values['check'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
    include('form.php');
}

else {
    $errors = FALSE;
    if(empty($_POST['fio'])) {
        setcookie('fio_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    elseif(!preg_match('/[а-яёА-ЯЁ]+/u',$_POST['fio'])){
        setcookie('fio_data_error','1',time()+24*60*60);
        $errors=TRUE;
    }
    else {
        setcookie('fio_value',$_POST['fio'],time()+365*24*60*60);
    }

    if(empty($_POST['email'])) {
        setcookie('email_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    elseif(!preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u',$_POST['email'])){
        setcookie('email_data_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('email_value',$_POST['email'],time()+365*24*60*60);
    }
    if(empty($_POST['birthday'])) {
        setcookie('bd_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    elseif($_POST['birthday']>date("Y-m-d")){
        setcookie('bd_data_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('bd_value',$_POST['birthday'],time()+365*24*60*60);
    }


    if(empty($_POST['radio2'])) {
        setcookie('gen_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('gen_value',$_POST['radio2'],time()+365*24*60*60);
    }
    if(empty($_POST['radio1'])) {
        setcookie('limbs_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else{
        setcookie('limbs_value',$_POST['radio1'],time()+365*24*60*60);
    }
    if(empty($_POST['textarea1'])) {
        setcookie('bio_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else{
        setcookie('bio_value',$_POST['textarea1'],time()+365*24*60*60);
    }
    if(empty($_POST['checkbox'])) {
        setcookie('check_error','1',time()+24*60*60);
        $errors = TRUE;
    }
    else {
        setcookie('check_value',$_POST['checkbox'],time()+365*24*60*60);
    }

    if($errors) {
        header('Location:index.php');
        exit();
    }
    else {
        setcookie('fio_error', '', 100000);
        setcookie('fio_data_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('email_data_error','',time()+100000);
        setcookie('bd_error','',100000);
        setcookie('bd_data_error','',100000);
        setcookie('gen_error','',100000);
        setcookie('limbs_error','',100000);
        setcookie('bio_error', '', 100000);
        setcookie('check_error','',100000);
    }

$user = 'u20963';
$pass = '3282783';
$db = new PDO('mysql:host=localhost;dbname=u20963', $user, $pass, array(PDO::ATTR_PERSISTENT => true));



$stmt = $db->prepare("INSERT INTO form (name, email,year,sex,lb,bio,checkbox) VALUES (:fio, :email,:birthday,:sex,:lb,:bio,:checkbox)");
$stmt -> execute(array('fio'=>$_POST['fio'], 'email'=>$_POST['email'],'birthday'=>$_POST['birthday'],'sex'=>$_POST['radio2'],'lb'=>$_POST['radio1'],'bio'=>$_POST['textarea1'],'checkbox'=>$_POST['checkbox']));


$stmt=$db->prepare("INSERT INTO abil(immortal,phasing,levitation) VALUES(:imm,:ph,:lv)");
$myselect=$_POST['select1'];
for($i=0;$i<3;$i++)
{
  if($myselect[$i]!=1)
  {
    $myselect[$i]=0;
  }
}
$stmt->execute(array('imm'=>$myselect[0],'ph'=>$myselect[1],'lv'=>$myselect[2]));
header('Location: ?save=1');
    setcookie('save', '1');

    header('Location:index.php');
}
